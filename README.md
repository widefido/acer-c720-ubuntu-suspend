# Acer C720 Ubuntu Suspend Fixes

- This is confirmed to be Working with the Celeron version of the C720 (2GB of RAM, 16GB SSD) and Ubuntu 14.04 LTS (upgraded to linux kernel 3.17.1)

## Installation

1. Copy the files from the git etc directory into their appropriate locations (or merge the modifications manually)
2. `update-grub` 
3. `reboot`
